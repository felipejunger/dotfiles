# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias ls='ls --color'

alias nvimrc='nvim $HOME/.config/nvim/init.vim'

export PATH=/home/klepz/Downloads/omnisharp-linux-x64:$PATH

export MSBuildSDKsPath=/opt/dotnet/sdk/$(dotnet --version)/Sdks

export PS1='\[\e[1;28m\]$\[\e[0m\] '

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Add environment variable COCOS_CONSOLE_ROOT for cocos2d-x
export COCOS_CONSOLE_ROOT="/home/klepz/Desktop/Cocos/tools/cocos2d-console/bin"
export PATH=$COCOS_CONSOLE_ROOT:$PATH

# Add environment variable COCOS_X_ROOT for cocos2d-x
export COCOS_X_ROOT="/home/klepz/Desktop"
export PATH=$COCOS_X_ROOT:$PATH

# Add environment variable COCOS_TEMPLATES_ROOT for cocos2d-x
export COCOS_TEMPLATES_ROOT="/home/klepz/Desktop/Cocos/templates"
export PATH=$COCOS_TEMPLATES_ROOT:$PATH

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Add environment variable NDK_ROOT for cocos2d-x
export NDK_ROOT="/home/klepz/Android/Ndk-r20"
export PATH=$NDK_ROOT:$PATH

# Add environment variable ANDROID_SDK_ROOT for cocos2d-x
export ANDROID_SDK_ROOT="/home/klepz/Android/Sdk"
export PATH=$ANDROID_SDK_ROOT:$PATH
export PATH=$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools:$PATH
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk

export VISUAL=nvim
export EDITOR="$VISUAL"

export TERM=tmux-256color
