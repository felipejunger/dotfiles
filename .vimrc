set nocompatible

call plug#begin('~/.config/nvim/plugins')
Plug 'tpope/vim-dispatch' 
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'klepz/vim-colors-paramount'
Plug 'felixfbecker/php-language-server', {'do': 'composer install && composer run-script parse-stubs'}
Plug 'tpope/vim-surround'
call plug#end()

syntax on
colorscheme paramount

let mapleader = ","
set shell=/bin/bash
set cursorline
filetype on
filetype indent on
filetype plugin indent on
set title
set number
set expandtab
set tabstop=8
set shiftwidth=8
set laststatus=2
set noshowmode
set t_Co=256
set background=light
set smarttab
set lbr
set tw=150
set ai "Auto indent
set si "Smart indent
set wildmenu
set wildignore=*.o,*~
set ww+=<,>
if has("win32")
        set wildignore+=.git\*,*.meta
else
        set wildignore+=*/.git/*,*.meta
endif
set noerrorbells
set novisualbell
set bs=2
set foldmethod=manual
set ttimeoutlen=1
set hidden
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=no
set encoding=utf-8
set fileencoding=utf-8
set belloff+=ctrlg
set completeopt-=menu,preview
set completeopt+=noinsert,noselect,longest,menuone
set showtabline=1  " Show tabline

" Use different cursor shape for each mode
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>
nmap <silent> <leader>vs :vsplit <CR>
nmap <silent> <leader>hs :split <CR>
nmap <silent> <A-=> :vertical resize +2 <CR> 
nmap <silent> <A--> :vertical resize -2 <CR>
nmap <silent> <A-]> :resize +2 <CR> 
nmap <silent> <A-[> :resize -2 <CR>
nnoremap <silent> <C-Left> :tabprevious<CR>
nnoremap <silent> <C-Right> :tabnext<CR>
nmap <silent> <C-l> :ls<CR>
map  <silent> <C-n> :NERDTreeToggle<CR>
nmap <silent> <C-p> :Files <CR>

function s:c_cpp_mode()

        nmap <silent><leader>df :LspDefinition <CR>
        nmap <silent><leader>cc :Dispatch! cocos run -p linux <CR>
        nnoremap <leader>mk :Dispatch! make -C build <CR>
        "run
        nnoremap <leader>rn :silent Dispatch! build/out <CR>

        "verbose run
        nnoremap <leader>vrn :!build/out <CR>

        map <silent><leader>f :pyf /usr/share/clang/clang-format.py<cr>


        if executable('clangd')

                augroup lsp_clangd
                        autocmd!
                        autocmd User lsp_setup call lsp#register_server({
                                                \ 'name': 'clangd',
                                                \ 'cmd': {server_info->['clangd']},
                                                \ 'whitelist': ['c', 'cpp', 'h'],
                                                \ })
                        setlocal omnifunc=lsp#complete
                augroup end
        endif

endfunction

function s:python_mode()


endfunction

"autocm FileType py call s:python_mode()

autocmd FileType c,cpp,h call s:c_cpp_mode()

let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

let g:lightline = {
                        \ 'colorscheme': 'one',
                        \ 'active': {
                        \   'left': [ [ 'mode', 'paste' ],
                        \             [ 'gitbranch','readonly', 'filename', 'modified' ] ]
                        \ },
                        \ 'component_function': {
                        \   'gitbranch': 'fugitive#head',
                        \ }
                        \}

let g:lightline.separator = {
                        \   'left': '', 'right': ''
                        \}
let g:lightline.subseparator = {
                        \   'left': '', 'right': '' 
                        \}

let g:lightline.tabline = {
                        \   'left': [ ['tabs'] ],
                        \   'right': [ [''] ]
                        \ }

let g:asyncomplete_auto_popup = 0

function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
                        \ pumvisible() ? "\<C-n>" :
                        \ <SID>check_back_space() ? "\<TAB>" :
                        \ asyncomplete#force_refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

let g:lsp_virtual_text_enabled = 0
let g:lsp_signs_enabled = 1         " enable signs
let g:lsp_diagnostics_echo_cursor = 1 " enable echo under cursor when in normal mode

if executable('pyls')
        au User lsp_setup call lsp#register_server({
                                \ 'name': 'pyls',
                                \ 'cmd': {server_info->['pyls']},
                                \ 'whitelist': ['python'],
                                \ 'workspace_config': {'pyls': {'plugins': {'pydocstyle': {'enabled': v:true}}}}
                                \ })
endif

au User lsp_setup call lsp#register_server({                                    
                        \ 'name': 'php-language-server',                                            
                        \ 'cmd': {server_info->['php', expand('~/.config/nvim/plugins/php-language-server/bin/php-language-server.php')]},
                        \ 'whitelist': ['php'],                                                     
                        \ })
